import Vue from 'vue';
import App from './App.vue';
import router from './router';
import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css';
import colors from 'vuetify/es5/util/colors';

Vue.use(Vuetify, {
  theme: {
    primary: colors.blue.darken4,
    secondary: colors.red.darken4,
    accent: colors.deepPurple.darken4,
    error: colors.red,
    info: colors.teal.darken1,
    success: colors.green.darken1,
    warning: colors.yellow.lighten1,
    white: colors.shades.white,
    black: colors.shades.black,
    light: colors.grey.lighten1,
  },
});

Vue.config.productionTip = false;

new Vue({
  router,
  render: (h) => h(App),
}).$mount('#app');

